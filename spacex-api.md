# SpaceX Crew API

## Task

Write a SpringBoot REST API that will expose "GET" method to give list of names of **"Active"** astronauts in **SpaceX** missions. Datasource you must use is https://api.spacexdata.com/v4/crew. Implement query parameter to give list of astronauts names by **"Agency"**.

## Example

http://localhost/spacex-crew will return JSON Array of names of all **"active"** astronauts.

http://localhost/spacex-crew?agency=NASA will return JSON Array of names of all **"active"** astronauts belonging to **"NASA"** agency.
     
