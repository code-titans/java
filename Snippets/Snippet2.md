### What is the output of the following code?

    class TestMe{
        int fun (int n) 
        {
            int result;
            result = fun (n - 1);
            return result;
        }
    } 
    class Driver{
        public static void main(String args[]) 
        {
            TestMe ib = new TestMe() ;
            System.out.print(ib.fun(12));
        }
    }
