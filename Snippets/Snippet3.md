### What is the output of the following code?

    class TestMe{
    public static void main(String args[])
    {
        String obj = "Hello";
        String obj1 = "TestMe";   
        String obj2 = "Hello";
        System.out.println(obj.equals(obj1) + " " + obj.equals(obj2));
    }
    }
