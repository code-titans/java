### What is the output of the following code?

    class TestMe{
        
        int calculate(int a, int b)
        {
            try{
                return a-b;
            }catch(Exception e){
                return a+b;
            }finally{
                return a*b;
            }
        }        
    }    
    class Driver{
        public static void main(String args[])
        {
            TestMe obj1 = new TestMe();
            int result = obj1.calculate(2, 3);
            System.out.println("Result: " + result);     
        }
    }
