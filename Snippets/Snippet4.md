### What is the output of the following code?

    class TestMe{
        public int num1;
        static int num2;
        void calculate(int a, int b)
        {
            num1 +=  a ;
            num2 +=  b;
        }        
    }    
    class Driver{
        public static void main(String args[])
        {
            TestMe obj1 = new TestMe();
            TestMe obj2 = new TestMe();   
            obj1.num1 = 0;
            obj1.num2 = 0;
            obj1.calculate(1, 2);
            obj2.num1 = 0;
            obj2.calculate(2, 3);
            System.out.println(obj1.num1 + " " + obj2.num2);     
        }
    }
